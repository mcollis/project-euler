/* File: projecteuler2.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/02/2012
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN_VALID_TARGET 2

int sumEvenFibs(int);

/* Function: main
 * --------------------------
 *	Takes one integer argument and finds the sum of all the
 *	even Fibonacci numbers less than or equal to the input.
 *	Returns an error if input is negative, or a non-integer.
 */
int main(int argc, const char *argv[]) 
{
	//Check that an input was indeed entered
	if(argv[1] != NULL) {
		int max = atoi(argv[1]);
		
		//Check that the input is valid
		if(max < MIN_VALID_TARGET) {
			printf("Error: invalid input.\nPlease check your input and try again.\n");
	        	return -1;
		} else {
			int sum = sumEvenFibs(max);
			printf("Answer: %d\n", sum);
		}

	} else {
        	printf("Error: no input.\nPlease input a single integer argument and try again.\n");
	        return -1;
    	}    
	
	return 0;
}

/* Function: sumEvenFibs
 * --------------------------
 *	Returns the sum of all even Fibonacci numbers
 *	less than or equal to a maximum input argument.
 */
int sumEvenFibs(int max) {
	int fib1 = 1;
	int fib2 = 1;
	int sum = 0;

	//Notice that every 3rd Fibonacci number is even
	int fib3 = fib1 + fib2;
	while(fib3 <= max) {
		sum += fib3;
		fib1 = fib2 + fib3;
		fib2 = fib3 + fib1;
		fib3 = fib1 + fib2;
	}

	return sum;
}
