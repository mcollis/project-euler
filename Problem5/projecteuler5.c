/* File: projecteuler5.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/04/2012
 */

#include <stdio.h>
#include <stdlib.h>

/* Function: main
 * --------------------------
 *	Calculates the product of the unique prime factorizations of each 
 *	of the integers between 1 and 20
 */
int main(int argc, const char *argv[]) 
{
	//Initialize the array of prime factors and the result variable
	int primes[] = {1, 2, 2, 2, 2, 3, 3, 5, 7, 11, 13, 17, 19};
	unsigned long long int answer = 1;
	
	//Calculate the product
	for(int i = 0; i < sizeof(primes)/sizeof(primes[0]); i++) {
		answer *= primes[i];
	}

	//Print the result
	printf("Answer: %lld\n", answer);
	
	return 0;
}
