/* File: projecteuler7.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/07/2012
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#define MAXN  1000000  /* maximum value of N */
#define GET(b) ((sieve[(b)>>5]>>((b)&31))&1)

int isPrime(int, uint32_t*);

/* Function: main
 * --------------------------
 *	Calculates the primes less than 1,000,000 and returns the 10,001st prime.
 */ 
int main(int argc, const char *argv[]) 
{
	//Uses an implementation of Eratosthenes' sieve to find all primes
	//less than 1,000,000
	//Source: www.algorithmist.com/index.php/Prime_Sieve_of_Eratosthenes.c

	int P1 = ceil(MAXN/64);
	int P2 = ceil(MAXN/2);
	int P3 = ceil(ceil(sqrt(MAXN))/2);
	uint32_t sieve[P1];
	uint32_t i, j, k, n;
	memset(sieve, 0, sizeof(sieve));
    	
	for (k = 1; k <= P3; k++) {
        	if (GET(k)==0) {
			for(j=2*k+1,i=2*k*(k+1);i<P2;i+=j) {
				sieve[i>>5]|=1<<(i&31);
			}
		}
	}

    	for (n = 0, i = 0; i <= MAXN; i++) {
        	if (isPrime(i, sieve)) {
			n++;
			if(n == 10001) {
				printf("The 10,001st prime is %d.\n", i);
				break;
			}
		}
	}

    return 0;
}

/* Function: isPrime
 * --------------------------
 *	Calculates if an input number is a prime
 */
int isPrime(int p, uint32_t* sieve)
{
	return (p==2) || (p>2 && (p&1)==1 && (GET((p-1)>>1)==0));
}
