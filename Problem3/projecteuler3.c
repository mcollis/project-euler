/* File: projecteuler3.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/02/2012
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MIN_VALID_TARGET 3
#define MAX_PRIME_FACTORS 10000

void getPrimeFactors(unsigned long long int, unsigned long long int*, int*);

/* Function: main
 * --------------------------
 *	Takes one unsigned long long integer argument and finds the largest 
 *	prime factor of that integer. Throws an error if the input is
 *	a non-integer or less than 3 (including for inputs larger
 *	than ULLONG_MAX).
 */
int main(int argc, const char *argv[]) 
{
	//Check that an input was indeed entered
	if(argv[1] != NULL) {
		unsigned long long int target = strtoull(argv[1], NULL, 0);
		
		//Check that the input is valid
		if(target < MIN_VALID_TARGET) {
			printf("Error: invalid input.\nPlease check your input and try again.\n");
	        	return -1;
		} else {
			//Initialize the array of unsigned long long ints to track the prime factors
			unsigned long long int* factors = malloc(sizeof(unsigned long long int) * MAX_PRIME_FACTORS);
			memset(factors, 0, sizeof(factors));

			//Initialize a counter to track the number of prime factors found
			//Call getPrimeFactors to retrieve the prime factors of the target
			int indexOfLastFactor = 0;
			getPrimeFactors(target, factors, &indexOfLastFactor);

			//Initialize the largest factor to be zero and search the array
			//of prime factors for the largest
			unsigned long long int largestFactor = 0;
			for(int i = 0; i <= indexOfLastFactor; i++) {
				if(factors[i] > largestFactor) {
					largestFactor = factors[i];
				}
			}

			//Free up the array and print the result
			free(factors);
			printf("Answer: %lld\n", largestFactor);
		}

	} else {
        	printf("Error: no input.\nPlease input a single integer argument and try again.\n");
	        return -1;
    	}    
	
	return 0;
}

/* Function: getPrimeFactors
 * --------------------------
 *	Fills in a table of unsigned long long ints with the prime factors found at 
 *	 sequential index values.
 */
void getPrimeFactors(unsigned long long int n, unsigned long long int* factors, int* index) {
	
	//End recursive call at base cases of 0, 1 or if the maximum number of prime
	//factors have been collected
	if (n == 0 || n == 1) return;
	if(*index >= MAX_PRIME_FACTORS) return;

	//Find the smallest integer greater than 1 that is a divisor of the input
	unsigned long long int divisor = 0;
 	for(unsigned long long int i = 2; i*i <= n; ++i) {
		if (n % i == 0) {
			divisor = i;
      			break;
    		}
  	}

	//No divisor was found, so the target, n, is a prime number
  	if (divisor == 0) {
    		factors[*index] = n;
		return;
  	}

  	//Or if a divisor was found: log the factor in the table; increment the index counter;
	//make a recursive call on the result of that division.
  	factors[*index] = divisor;
	(*index)++;
  	getPrimeFactors(n / divisor, factors, index);
}
