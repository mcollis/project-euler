/* File: projecteuler9.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/07/2012
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define MIN_TARGET_ALLOWED 12

/* Function: main
 * --------------------------
 *	Calculates the first pythagorean triple whose values sum
 *	to an input integer. Throws an error on non-integer inputs or
 *	targets less than 12 (3-4-5 triple).
 */ 
int main(int argc, const char *argv[]) 
{

	//Check that an input was indeed entered
	if(argv[1] != NULL) {
		int target = atoi(argv[1]);
		
		//Check that the input is valid
		if(target < MIN_TARGET_ALLOWED) {
			printf("Error: invalid input.\nArgument must be an integer >12. Please try again.\n");
	        	return -1;
		} else {
			int product = 0;
			
			//Loop over the possible values of A and B and check if the remainder
			//up to the target sum makes a Pythagorean triple
			for(int a = 3; a < target-3; a++) {
				for(int b = a+1; b < target-a-1; b++) {
					int c = target - a - b;
					if((c*c) == ((a*a)+(b*b))) {
						product = a * b * c;
						printf("Pythagorean triple found: %d-%d-%d, product = %d\n", a, b, c, product);
						return 0;
					}
				}
			}
		}

	} else {
		printf("Error: no input detected.\nPlease input a single integer argument and try again.\n");
	        return -1;
	}

    return 0;
}




