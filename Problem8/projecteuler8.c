/* File: projecteuler8.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/07/2012
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Function: main
 * --------------------------
 *	Calculates greatest product of 5 consecutive integers from a
 *	hard-coded input string.
 */ 
int main(int argc, const char *argv[]) 
{
	//Input string
	char* input = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450\0";

	//Initialize the product and the number of characters in the input
	int product = 1, maxProduct = 1;
	int numCharacters = strlen(input);
	
	for(int i = 0; i < numCharacters; i++) {
		
		//If at least 5 numbers have been retrieved for the product,
		//remove the first one in the sequence
		if(i >=5) {
			char charToRemove[2];
			charToRemove[0] = input[i-5];
			charToRemove[1] = '\0';
			
			//Check if the character being removed is a zero
			//If not, proceed by dividing the current product by
			//first character in the sequence
			if(atoi(charToRemove) !=0) {
				product /= atoi(charToRemove);
			
			//If it is a zero, reinitialize the product and 
			//reacquire the four other numbers in the sequence
			//that should make up the product at this point
			} else {
				product = 1;
				for(int j = 4; j>0; j--) {
					char charToAdd[2];
					charToAdd[0] = input[i-j];
					charToAdd[1] = '\0';
					product *= atoi(charToAdd);
				}
			}
		}
		
		//Add the next character to the product
		char charToAdd[2];
		charToAdd[0] = input[i];
		charToAdd[1] = '\0';
		product *= atoi(charToAdd); 

		//Check if the new product is larger than the largest yet
		//recorded and update as necessary
		if(product > maxProduct) {
			maxProduct = product;
		}
	}
	
	//Print the answer
	printf("Answer: %d\n", maxProduct);	

	return 0;
}

