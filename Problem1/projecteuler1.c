/* File: projecteuler1.c
 * ------------------
 * Name: Michael Collis
 * Date: 11/30/2012
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN_VALID_TARGET 3

int sumDivisible(int, int);

/* Function: main
 * --------------------------
 *	Takes one integer argument and finds the sum of all the
 *	multiples of 3 and 5 less than the input. Returns an error
 *	if input is negative, or a non-integer.
 */
int main(int argc, const char *argv[]) 
{
	//Check that an input was indeed entered
	if(argv[1] != NULL) {
		int max = atoi(argv[1]);
		
		//Check that the input is valid
		if(max < MIN_VALID_TARGET) {
			printf("Error: invalid input.\nPlease check your input and try again.\n");
	        	return -1;
		} else {
			int sum = sumDivisible(3, max) + sumDivisible(5, max) - sumDivisible(15, max);
			printf("Answer: %d\n", sum);
		}

	} else {
        	printf("Error: no input.\nPlease input a single integer argument and try again.\n");
	        return -1;
    	}    
	
	return 0;
}

/* Function: sumDivisible
 * --------------------------
 *	Returns the sum of all integers less than a maximum that 
 *	are divisible by a given input integer.
 */
int sumDivisible(int n, int max) {
	double p = (max-1)/n;
	return (int) (n*p*(p+1)/2);
}
