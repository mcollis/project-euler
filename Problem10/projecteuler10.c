/* File: projecteuler7.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/07/2012
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#define GET(b) ((sieve[(b)>>5]>>((b)&31))&1)
#define MIN_TARGET_ALLOWED 2

int isPrime(int, uint32_t*);

/* Function: main
 * --------------------------
 *	Calculates the primes less than a user input integer and returns their sum.
 *	Throws an error if the input is less than 2 or a non-integer.
 */ 
int main(int argc, const char *argv[]) 
{
	//Uses an implementation of Eratosthenes' sieve to find all primes
	//less than an input value
	//Source: www.algorithmist.com/index.php/Prime_Sieve_of_Eratosthenes.c

	//Check that an input was indeed entered
	if(argv[1] != NULL) {
		int target = atoi(argv[1]);
		
		//Check that the input is valid
		if(target < MIN_TARGET_ALLOWED) {
			printf("Error: invalid input.\nArgument must be an integer >1. Please try again.\n");
	        	return -1;
		} else {
	
			int P1 = ceil(target/64);
			int P2 = ceil(target/2);
			int P3 = ceil(ceil(sqrt(target))/2);
			uint32_t sieve[P1];
			uint32_t i, j, k;
			memset(sieve, 0, sizeof(sieve));
    	
			for (k = 1; k <= P3; k++) {
        			if (GET(k)==0) {
					for(j=2*k+1,i=2*k*(k+1);i<P2;i+=j) {
						sieve[i>>5]|=1<<(i&31);
					}
				}
			}

			unsigned long long int sum = 0;
    			for (i = 0; i <= target; i++) {
        			if (isPrime(i, sieve)) {
					sum += i;
				}
			}

			printf("Answer: %lld\n", sum);
		}
	
	} else {
		printf("Error: no input detected.\nPlease input a single integer argument and try again.\n");
	        return -1;
	}

	return 0;
}

/* Function: isPrime
 * --------------------------
 *	Calculates if an input number is a prime
 */
int isPrime(int p, uint32_t* sieve)
{
	return (p==2) || (p>2 && (p&1)==1 && (GET((p-1)>>1)==0));
}
