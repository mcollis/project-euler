/* File: projecteuler4.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/03/2012
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int isPalindromic(int);

/* Function: main
 * --------------------------
 *	Finds the largest palindrome number made from the product
 *	two three-digit numbers
 */
int main(int argc, const char *argv[]) 
{

	//Initialize the largest palindrome and iteratively search
	//for the largest palindromic number, starting at the top.
	int largestPalindrome = 0, multiplicand1 = 0, multiplicand2 = 0;
	for(int i = 999; i >= 100; i--) {
		
		//Check if there are any possible larger palindromic numbers to be found
		if((i * 999) < largestPalindrome) {
			break;
		}	

		for(int j = 999; j >= i; j--) {
			//Check if the two numbers would produce a number larger than the
			//current largest palindromic number, then check if it is indeed
			//palindromic (avoids calling isPalindromic whenever possible
			//Update the largest palindrome when applicable
			if(i*j > largestPalindrome && isPalindromic(i * j) == 1) {				
				largestPalindrome = i * j;
				multiplicand1 = i;
				multiplicand2 = j;			
				break;
			}
		}
	}

	//Print the result
	printf("Answer: %d * %d = %d\n", multiplicand1, multiplicand2, largestPalindrome);
	
	return 0;
}

/* Function: isPalindromic
 * --------------------------
 *	Calculates if a number is a palindromic number
 *	Returns a 1 if it is, and a 0 otherwise
 */
int isPalindromic(int input) {
	int numDigits = (int) ceil(log10(input + 1));
	//Iterate over the digits from the outermost numbers
	//towards the middle and check if the digits match
	for (int n = 0; n < numDigits / 2; n++) {
        	if ((input / (int) pow(10, n)) % 10 !=
            	(input / (int) pow(10, numDigits - n - 1)) % 10) {
            		return 0;
		}
	}
    return 1;
}
