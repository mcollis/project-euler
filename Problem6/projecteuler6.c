/* File: projecteuler6.c
 * ------------------
 * Name: Michael Collis
 * Date: 12/05/2012
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN_VALID_TARGET 1

/* Function: main
 * --------------------------
 *	Calculates the difference between the sum of squares of first N
 *	natural numbers and the square of their sum
 */
int main(int argc, const char *argv[]) 
{
	//Check that an input was indeed entered
	if(argv[1] != NULL) {
		int target = strtol(argv[1], NULL, 0);
		
		//Check that the input is valid
		if(target < MIN_VALID_TARGET) {
			printf("Error: invalid input.\nPlease check your input and try again.\n");
	        	return -1;
		} else {
			//Initialize 64-bit integers to hold the square of the sum and sum of the squares
			unsigned long long int sqSum = 0;
			unsigned long long int sumSq = 0;

			//Loop over the first natural numbers up to and including the target input
			for(int i = 1; i <= target; i++) {
				sqSum += i;
				sumSq += i * i;
			}

			//Calculate and print the result
			sqSum = sqSum * sqSum;
			unsigned long long int difference = sqSum - sumSq;
			printf("Answer: %lld\n", difference);
		}

	} else {
        	printf("Error: no input.\nPlease input a single integer argument and try again.\n");
	        return -1;
    	}   
}
